namespace CarDealership.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class JoinTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DealershipManufacturers", "Dealership_Id", "dbo.Dealerships");
            DropForeignKey("dbo.DealershipManufacturers", "Manufacturer_Id", "dbo.Manufacturers");
            DropIndex("dbo.DealershipManufacturers", new[] { "Dealership_Id" });
            DropIndex("dbo.DealershipManufacturers", new[] { "Manufacturer_Id" });
            AddColumn("dbo.Dealerships", "Manufacturer_Id", c => c.Int());
            CreateIndex("dbo.Dealerships", "Manufacturer_Id");
            AddForeignKey("dbo.Dealerships", "Manufacturer_Id", "dbo.Manufacturers", "Id");
            DropTable("dbo.DealershipManufacturers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.DealershipManufacturers",
                c => new
                    {
                        Dealership_Id = c.Int(nullable: false),
                        Manufacturer_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Dealership_Id, t.Manufacturer_Id });
            
            DropForeignKey("dbo.Dealerships", "Manufacturer_Id", "dbo.Manufacturers");
            DropIndex("dbo.Dealerships", new[] { "Manufacturer_Id" });
            DropColumn("dbo.Dealerships", "Manufacturer_Id");
            CreateIndex("dbo.DealershipManufacturers", "Manufacturer_Id");
            CreateIndex("dbo.DealershipManufacturers", "Dealership_Id");
            AddForeignKey("dbo.DealershipManufacturers", "Manufacturer_Id", "dbo.Manufacturers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DealershipManufacturers", "Dealership_Id", "dbo.Dealerships", "Id", cascadeDelete: true);
        }
    }
}

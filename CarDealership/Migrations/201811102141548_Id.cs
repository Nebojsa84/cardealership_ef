namespace CarDealership.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Id : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Dealerships", "CarId");
            DropColumn("dbo.Dealerships", "ManufacturerId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Dealerships", "ManufacturerId", c => c.Int(nullable: false));
            AddColumn("dbo.Dealerships", "CarId", c => c.Int(nullable: false));
        }
    }
}

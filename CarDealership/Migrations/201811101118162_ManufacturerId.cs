namespace CarDealership.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ManufacturerId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Cars", "Manufacturer_Id", "dbo.Manufacturers");
            DropIndex("dbo.Cars", new[] { "Manufacturer_Id" });
            RenameColumn(table: "dbo.Cars", name: "Manufacturer_Id", newName: "ManufacturerId");
            AlterColumn("dbo.Cars", "ManufacturerId", c => c.Int(nullable: false));
            CreateIndex("dbo.Cars", "ManufacturerId");
            AddForeignKey("dbo.Cars", "ManufacturerId", "dbo.Manufacturers", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cars", "ManufacturerId", "dbo.Manufacturers");
            DropIndex("dbo.Cars", new[] { "ManufacturerId" });
            AlterColumn("dbo.Cars", "ManufacturerId", c => c.Int());
            RenameColumn(table: "dbo.Cars", name: "ManufacturerId", newName: "Manufacturer_Id");
            CreateIndex("dbo.Cars", "Manufacturer_Id");
            AddForeignKey("dbo.Cars", "Manufacturer_Id", "dbo.Manufacturers", "Id");
        }
    }
}

namespace CarDealership.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Dealerships", "Manufacturer_Id", "dbo.Manufacturers");
            DropIndex("dbo.Dealerships", new[] { "Manufacturer_Id" });
            DropColumn("dbo.Dealerships", "Manufacturer_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Dealerships", "Manufacturer_Id", c => c.Int());
            CreateIndex("dbo.Dealerships", "Manufacturer_Id");
            AddForeignKey("dbo.Dealerships", "Manufacturer_Id", "dbo.Manufacturers", "Id");
        }
    }
}

namespace CarDealership.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class First : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Model = c.String(nullable: false),
                        Year = c.String(),
                        Color = c.String(),
                        Volume = c.Double(nullable: false),
                        Dealership_Id = c.Int(),
                        Manufacturer_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Dealerships", t => t.Dealership_Id)
                .ForeignKey("dbo.Manufacturers", t => t.Manufacturer_Id)
                .Index(t => t.Dealership_Id)
                .Index(t => t.Manufacturer_Id);
            
            CreateTable(
                "dbo.Manufacturers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        State = c.String(maxLength: 50),
                        City = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Dealerships",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        State = c.String(maxLength: 50),
                        City = c.String(maxLength: 50),
                        PIB = c.String(nullable: false, maxLength: 50),
                        Address = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DealershipManufacturers",
                c => new
                    {
                        Dealership_Id = c.Int(nullable: false),
                        Manufacturer_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Dealership_Id, t.Manufacturer_Id })
                .ForeignKey("dbo.Dealerships", t => t.Dealership_Id, cascadeDelete: true)
                .ForeignKey("dbo.Manufacturers", t => t.Manufacturer_Id, cascadeDelete: true)
                .Index(t => t.Dealership_Id)
                .Index(t => t.Manufacturer_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cars", "Manufacturer_Id", "dbo.Manufacturers");
            DropForeignKey("dbo.DealershipManufacturers", "Manufacturer_Id", "dbo.Manufacturers");
            DropForeignKey("dbo.DealershipManufacturers", "Dealership_Id", "dbo.Dealerships");
            DropForeignKey("dbo.Cars", "Dealership_Id", "dbo.Dealerships");
            DropIndex("dbo.DealershipManufacturers", new[] { "Manufacturer_Id" });
            DropIndex("dbo.DealershipManufacturers", new[] { "Dealership_Id" });
            DropIndex("dbo.Cars", new[] { "Manufacturer_Id" });
            DropIndex("dbo.Cars", new[] { "Dealership_Id" });
            DropTable("dbo.DealershipManufacturers");
            DropTable("dbo.Dealerships");
            DropTable("dbo.Manufacturers");
            DropTable("dbo.Cars");
        }
    }
}

namespace CarDealership.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m6 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.DealershipsManufacturers", newName: "Contracts");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Contracts", newName: "DealershipsManufacturers");
        }
    }
}

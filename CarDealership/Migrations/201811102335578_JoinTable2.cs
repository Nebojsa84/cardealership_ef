namespace CarDealership.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class JoinTable2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DealershipsManufacturers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ManufacturerId = c.Int(nullable: false),
                        DealershipId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Dealerships", t => t.DealershipId, cascadeDelete: false)
                .ForeignKey("dbo.Manufacturers", t => t.ManufacturerId, cascadeDelete: false)
                .Index(t => t.ManufacturerId)
                .Index(t => t.DealershipId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DealershipsManufacturers", "ManufacturerId", "dbo.Manufacturers");
            DropForeignKey("dbo.DealershipsManufacturers", "DealershipId", "dbo.Dealerships");
            DropIndex("dbo.DealershipsManufacturers", new[] { "DealershipId" });
            DropIndex("dbo.DealershipsManufacturers", new[] { "ManufacturerId" });
            DropTable("dbo.DealershipsManufacturers");
        }
    }
}

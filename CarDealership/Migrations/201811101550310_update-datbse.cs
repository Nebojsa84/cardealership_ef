namespace CarDealership.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedatbse : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dealerships", "CarId", c => c.Int(nullable: false));
            AddColumn("dbo.Dealerships", "ManufacturerId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Dealerships", "ManufacturerId");
            DropColumn("dbo.Dealerships", "CarId");
        }
    }
}

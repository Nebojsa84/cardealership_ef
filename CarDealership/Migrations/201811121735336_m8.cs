namespace CarDealership.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m8 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Cars", "ManufacturerId", "dbo.Manufacturers");
            DropIndex("dbo.Cars", new[] { "ManufacturerId" });
            RenameColumn(table: "dbo.Cars", name: "Dealership_Id", newName: "DealershipId");
            RenameIndex(table: "dbo.Cars", name: "IX_Dealership_Id", newName: "IX_DealershipId");
            AlterColumn("dbo.Cars", "ManufacturerId", c => c.Int());
            CreateIndex("dbo.Cars", "ManufacturerId");
            AddForeignKey("dbo.Cars", "ManufacturerId", "dbo.Manufacturers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cars", "ManufacturerId", "dbo.Manufacturers");
            DropIndex("dbo.Cars", new[] { "ManufacturerId" });
            AlterColumn("dbo.Cars", "ManufacturerId", c => c.Int(nullable: false));
            RenameIndex(table: "dbo.Cars", name: "IX_DealershipId", newName: "IX_Dealership_Id");
            RenameColumn(table: "dbo.Cars", name: "DealershipId", newName: "Dealership_Id");
            CreateIndex("dbo.Cars", "ManufacturerId");
            AddForeignKey("dbo.Cars", "ManufacturerId", "dbo.Manufacturers", "Id", cascadeDelete: true);
        }
    }
}

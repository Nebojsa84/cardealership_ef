﻿using CarDealership.Models;
using CarDealership.ViewModel;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarDealership.Controllers
{
    public class CarController : Controller
    {
        public DealershipDbContext db { get; set; } = new DealershipDbContext();
       
        // GET: Car
        public ActionResult Index()
        {
            CarViewModel cvm = new CarViewModel()
            {
                Car = null,
                Cars = db.Cars.Include(c => c.Manufacturer).ToList()
            };
            
            return View(cvm);
        }

       
        public ActionResult Create()
        {
            CarManufacturerViewModel cmvm = new CarManufacturerViewModel()
            {
                Car = null,
                Manufacturers = db.Manufacturers.ToList()
            };
          
            return View(cmvm);
        }
        [HttpPost]
        public ActionResult Create(Car car)
        {
            try
            {
                db.Cars.Add(car);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch 
            {
                CarManufacturerViewModel cmvm = new CarManufacturerViewModel()
                {
                    Car = car,
                    Manufacturers = db.Manufacturers.ToList()
                };

                return View(cmvm);

            }
          
        }
    }
}
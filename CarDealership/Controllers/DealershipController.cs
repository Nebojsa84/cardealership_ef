﻿using CarDealership.Models;
using CarDealership.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarDealership.Controllers
{
    public class DealershipController : Controller
    {
        public DealershipDbContext db { get; set; } = new DealershipDbContext();
       
        public ActionResult Index()
        {
            var dealership = db.Dealerships.ToList();
            return View(dealership);
        }

        public ActionResult Create()
        {          
            return View();
        }

        [HttpPost]
        public ActionResult Create(Dealership deal )
        {
            try
            {              
                db.Dealerships.Add(deal);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {                            
                return View(deal);
            }
        }

        public ActionResult Details(int id)
        {
            var dealership = db.Dealerships
                                .Where(d => d.Id == id)
                                .Include(d => d.Cars)
                                .Include(d=>d.Contracts.Select(c=>c.Manufacturer))
                                .FirstOrDefault();

            return View(dealership);

        }

        public ActionResult Contract(int id)
        {
            DealershipManufacturersViewModel dmvm = new DealershipManufacturersViewModel();
            dmvm.Manufacturers = db.Manufacturers.ToList();
            dmvm.DealershipId = id;
            return View(dmvm);
        }
        [HttpPost]
        public ActionResult Contract(DealershipManufacturersViewModel dmvm)
        {
            try
            {
                Contracts dm = new Contracts()
                {
                    DealershipId = dmvm.DealershipId,
                    ManufacturerId = dmvm.ManufacturerId
                };

                db.Contracts.Add(dm);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                dmvm.Manufacturers = db.Manufacturers.ToList();
                return View(dmvm);
            }
        }

        public ActionResult AddCar(int id)
        {
            var contract = db.Contracts // U veznoj tabeli pronadje sve ugovore koje prodavnica ima
                             .Where(c => c.DealershipId == id)
                             .Select(c => c.ManufacturerId)
                             .ToList();

            List<Car> cars = new List<Car>();

            foreach (var c in contract) // popunjava listu sa automobilima od onih proizvodjaca sa kojima prodavnica ima ugovor
            {                               //i ignorise one koji su vec dodati
                cars.AddRange(db.Cars
                          .Where(m => m.ManufacturerId == c && m.DealershipId !=id ).ToList());
            }

            DealershipCarsViewModel model = new DealershipCarsViewModel()
            {
                DealershipId = id,
                Cars = cars
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult AddCar(DealershipCarsViewModel model)
        {
            var car = db.Cars.Find(model.CarId);
            var dealership = db.Dealerships.Find(model.DealershipId);
            car.Dealership = dealership;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
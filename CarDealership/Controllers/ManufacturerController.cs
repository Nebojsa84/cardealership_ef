﻿using CarDealership.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarDealership.Controllers
{
    public class ManufacturerController : Controller
    {
        public DealershipDbContext db { get; set; } = new DealershipDbContext();
        // GET: Manufacturer
        public ActionResult Index()
        {
            var manufacturers = db.Manufacturers.ToList();

            return View(manufacturers);
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Manufacturer manu)
        {
            try
            {
                db.Manufacturers.Add(manu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {

                return View();
            }
        }
    }
}
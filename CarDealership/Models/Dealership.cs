﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarDealership.Models
{
    public class Dealership
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(50)]
        public string State { get; set; }
        [StringLength(50)]
        public string City { get; set; }
        [Required]        
        [StringLength(50)]
        public string PIB { get; set; }
        [StringLength(50)]
        public string Address { get; set; }        
        public ICollection<Car> Cars { get; set; }
        public ICollection<Contracts>Contracts { get; set; }



    }
}
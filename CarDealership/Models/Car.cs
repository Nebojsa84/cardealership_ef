﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarDealership.Models
{
    public class Car
    {
        public int Id { get; set; }
        [Required]
        public string Model { get; set; }
        public string Year { get; set; }
        public string Color { get; set; }
        [Required]
        public double? Volume { get; set; }
        public int? ManufacturerId { get; set; }
        public Manufacturer Manufacturer { get; set; }
        public int? DealershipId { get; set; }
        public Dealership Dealership { get; set; }

    }   
}
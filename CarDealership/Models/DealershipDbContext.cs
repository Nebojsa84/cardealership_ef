﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CarDealership.Models
{
    public class DealershipDbContext:DbContext
    {
        public DealershipDbContext()
        {
            
        }
        public DbSet<Contracts> Contracts { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Dealership> Dealerships { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
    }
}
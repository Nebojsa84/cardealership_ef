﻿using CarDealership.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarDealership.ViewModel
{
    public class DealershipManufacturersViewModel
    {
        public int DealershipId { get; set; }
        public Dealership Dealership { get; set; }
        public int ManufacturerId { get; set; }
        public ICollection<Manufacturer> Manufacturers { get; set; }
    }
}
﻿using CarDealership.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarDealership.ViewModel
{
    public class DealershipCarsViewModel
    {
        public int DealershipId { get; set; }
        public int CarId { get; set; }
        public ICollection<Car> Cars { get; set; }
    }
}
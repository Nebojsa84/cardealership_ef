﻿using CarDealership.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarDealership.ViewModel
{
    public class CarManufacturerViewModel
    {
        public Car Car { get; set; }
        public ICollection<Manufacturer> Manufacturers { get; set; }

    }
}